<?php

namespace Drupal\delete_book\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BookDeleteConfirmationForm.
 */
class BookDeleteConfirmationForm extends ConfirmFormBase {
  /**
   * The book title.
   *
   * @var string
   */
  protected $bookTitle;

  /**
   * The book id.
   *
   * @var int
   */
  protected $bookId;

  /**
   * Drupal\book\BookManagerInterface definition.
   *
   * @var \Drupal\book\BookManagerInterface
   */
  protected $bookManager;

  /**
   * Drupal\book\BookManagerInterface definition.
   *
   * @var \Drupal\book\BookOutlineStorageInterface
   */
  protected $bookOutlineStorage;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Database Service Object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->bookManager = $container->get('book.manager');
    $instance->bookOutlineStorage = $container->get('book.outline_storage');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->messenger = $container->get('messenger');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'book_delete_confirmation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {

    return $this->t('Are you sure you want to delete the %bookTitle?', ['%bookTitle' => $this->bookTitle]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('delete_book.book_controller_getBookList');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This action cannot be undone. The Book and it\'s children will be removed. are you sure?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $book_id = NULL) {
    $this->bookId = $book_id;
    $node = Node::load($book_id);
    $this->bookTitle = $node->getTitle();
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $bookChildrenNids = $this->getBookChildrenNode($this->bookId);
    $storage = $this->entityTypeManager->getStorage('node');
    foreach (array_chunk($bookChildrenNids, 50) as $chunk) {
      $nodes = $storage->loadMultiple($chunk);
      $storage->delete($nodes);
    }
//    $mainBook = $storage->load($this->bookId);
//    $mainBook->delete();
    $this->messenger()->addMessage($this->t('The  %bookTitle has been deleted.', ['%bookTitle' => $this->bookTitle]));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * @param int $bookId
   *
   * @return int[]|string[]
   */
  private function getBookChildrenNode(int $bookId){
    $query = $this->connection->select('book',"b");
    $query->addField("b","nid");
    $query->condition('bid', $bookId);
    $query->orderBy("nid","DESC");

    return $query->execute()->fetchAll(\PDO::FETCH_COLUMN, 0);
  }

}
